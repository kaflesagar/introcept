@include('includes/header')
<div class="container">
    <a href="{{route('show_user_form')}}" class="btn btn-primary btn-sm mt-2 mb-3">Add User Record</a>
    <table class="table table-striped" id="viewAllUser">
        <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Address</th>
            <th>Gender</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Nationality</th>
            <th>Date of birth</th>
            <th>Education Background</th>
            <th>Mode of Contact</th>
            <th>Details</th>
        </tr>
        </thead>
        <tbody>
        @foreach($datas as $data)
            <tr>
                <td>{{$data[0]}}</td>
                <td>{{$data[1]}}</td>
                <td>{{$data[2]}}</td>
                <td>{{$data[3]}}</td>
                <td>{{$data[4]}}</td>
                <td>{{$data[5]}}</td>
                <td>{{$data[6]}}</td>
                <td>{{$data[7]}}</td>
                <td>{{$data[8]}}</td>
                <td>{{$data[9]}}</td>
                <td><a href="{{route('view_single_user',$data[0])}}"  target="_blank">View Details</a></td>
            
            </tr>
        @endforeach
        </tbody>

    </table>
</div>
@include('includes/footer')
<script>
    $('#viewAllUser').DataTable(
        {
            columnDefs: [
                {
                    targets: [0],
                    visible: true,
                    searchable: true,
                    sortable: false
                }
            ],
            order: [
                [0, 'desc'],
            ]
        }
    );
</script>
</body>
</html>