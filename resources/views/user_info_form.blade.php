@include('includes/header')

<div class="container mt-3 col-sm-8">
    <h3>UserInfo Record Form:</h3>
    @if(session()->has('success'))
    <!-- Log data to logentries after sucessfull insertion of User  -->

    <script>
        // log something
        LE.log("Client is created sucessfully!!");
        </script>
    <div class="alert alert-success">
        {{ session()->get('success') }} 
    </div>
@endif
    <a href="{{route('view_all_user')}}" class="btn btn-primary btn-sm mt-2 mb-3">View User Record</a>
    <div class="col-sm-12  mt-3">
        <form id="userForm" action="{{route('store_user_info')}}" method="post">
            {{csrf_field()}}
            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Name</label>
                    <input type="text" class="form-control" placeholder="Enter Name" name="name"
                          required>
                    @if(!empty($errors->first('name')))
                        <div id="name-error" class="errorClass">This field is required</div>@endif
                </div>

                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Address</label>
                    <input type="text" class="form-control" placeholder="Enter Address" name="address"
                           required>
                    @if(!empty($errors->first('address')))
                        <div id="name-error" class="errorClass">This field is required</div>@endif

                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Phone</label>
                    <input type="text" class="form-control" placeholder="Enter Phone" name="phone"
                           required>
                    @if(!empty($errors->first('phone')))
                        <div id="name-error" class="errorClass">{{ $errors->first('phone') }}</div>@endif

                </div>
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Gender</label><br>
                    <label class="form-check-label">
                        <input class="form-check-input radio" type="radio" name="gender" value="male"
                            
                        <span class="ml-1 activate">Male</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input class="form-check-input radio" type="radio" name="gender" value="female"
                               
                        <span class="ml-1 activate">Female</span>
                    </label>
                    @if(!empty($errors->first('gender')))
                        <div id="name-error" class="errorClass">This field is required</div>@endif

                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Email</label>
                    <input type="email" class="form-control" placeholder="Enter Email"
                           name="email" required>
                    @if(!empty($errors->first('email')))
                        <div id="name-error" class="errorClass">{{ $errors->first('email') }}</div>@endif

                </div>
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Nationality</label>
                    <input type="text" class="form-control" placeholder="Enter Nationality" name="nationality"
                          required>
                    @if(!empty($errors->first('nationality')))
                        <div id="name-error" class="errorClass">This field is required</div>@endif

                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Date of birth</label>
                    <input type="date" class="form-control" name="dob"
                           placeholder="Enter Date of birth" data-inputmask="'alias': 'mm/dd/yyyy'"
                           data-mask value="" required>
                    @if(!empty($errors->first('dob')))
                        <div id="name-error" class="errorClass">This field is required</div>@endif

                </div>
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Mode of Contact</label><br>
                    <label class="form-check-label">
                        <input class="form-check-input radio" type="radio" name="mode_of_contact" value="email"
                               
                        <span class="ml-1">Email</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input class="form-check-input radio" type="radio" name="mode_of_contact" value="phone"
                              
                        <span class="ml-1">Phone</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input class="form-check-input radio" type="radio" name="mode_of_contact" value="none"
                             
                        <span class="ml-1">None</span>
                    </label>
                    @if(!empty($errors->first('mode_of_contact')))
                        <div id="name-error" class="errorClass">This field is required</div>@endif

                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label class="label font-weight-bold">Education Background</label>
                    <textarea rows="4" class="form-control" cols="60" name="education_background"
                              placeholder="Enter education background" required></textarea>
                    @if(!empty($errors->first('education_background')))
                        <div id="name-error" class="errorClass">This field is required</div>@endif

                </div>
            </div>
           
                <input type="submit" class="btn btn-primary" value="Add">
        
        </form>
    </div>
</div>

@include('includes/footer')
<script>
    //form validation
    var formValid = $('#userForm');
    formValid.validate({
        debug: false,
        errorClass: "errorClass",
        errorElement: "div",
        rules: {
            name: {
                required: true,
                digits: false
            },
            phone: {
                digits: true,
                required: true
            }
        },
        errorPlacement: function (error, element) {
            $(error).insertAfter(element);
        }
    });
</script>
<style>
    .errorClass {
        color: #FF0000; /* red */
    }
</style>
