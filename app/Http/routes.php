<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'UserInformationController@create')->name('show_user_form');
Route::post('store', 'UserInformationController@store')->name('store_user_info');
Route::get('view', 'UserInformationController@showAllUser')->name('view_all_user');
Route::get('viewDetails/{id}', 'UserInformationController@details')->name('view_single_user');


